# core_arch


## Name
1Core Architecture

## Description
This repository will track the decisions and modifications introduce in the 1Core solution at the architecture level. This includes a matrix of containers and corresponding technical requirements.  

## Visuals
All the resources in this repository are linked to make the content browsable. A good place to start is the System Level 2 diagram
[System Level 1](./c4_model/system_level_1.md)

## Installation
No installation required. This repo is based on simple Markdown files and plantuml diagrams

## Usage
The team manages architecture through 4 different resources. All resources are located in usual maven resources location:

* [C4 Diagrams](./c4_model): Provided Level 1 and Level 2 diagrams
* [Tech Requirements](./tech_requirements/tech_requirement_list.md): A formal definition of tech requirements that will be linked to containers later.
* [Containers Matrices](./containers): Container definition with links to tech requirements and diagrams
* [ADR Records](./adr): The log of ADR records as defined in [ADR](https://github.com/joelparkerhenderson/architecture_decision_record)

## Support
Contact Enterprise Architect team

## Roadmap
* Add Voice calls via VoLTE
* Add SMS via SMSc

## Contributing
All the content is linked using Markdown hyperlinks with relative paths, to allow navigation throughout the content.Please always try to
introduce as much linking as possible:

* Writing NFR
  There is a [NFR template](./nfrs/nfr_template.md) you may use to fork new requirements. The Id just follows an easy convention TR-<seqNum>.
* Writing Container Matrices
  There is a [container template](./containers/container_template.md) you may use to fork new containers. The file should be named with container id.
  Containers should provide links to corresponding diagrams and requirements.
* Creating Diagrams
  We use plantuml. Please link to other resources correspondingly. In particular diagrams should link to container resources.
* ADR records
  We use [ADR](https://github.com/joelparkerhenderson/architecture_decision_record). Please link to other resources correspondingly.

Any change in architecture should be introduced through regular Git workflow, using separate branch, and PR. Only when PR is approved, the merge
into master will happen.
## Glossary
[Confluence Glossary](https://1ncegmbh.atlassian.net/wiki/spaces/TEC/pages/1067286592/Abbreviations+Terms)

## Project status
Baseline of current solution status
