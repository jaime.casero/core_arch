# Name
Retention policy for PII sensitive fields is 30 days
# Id
TR-1
# Description
After that the values of the PII fields are replaced with a PII_retention_expired  placeholder.
# Example
Retrieving a Call detail records older than 30 days, must show PII placeholder is PSTN number field.
# Source
[SDR Phase 1 Reqs](https://docs.google.com/document/d/1_6I4woZrTQKsvXoujpv9TqtRz6IcMXZ7vfFmq8nB4L8/edit)
# Related rules
* [Related Resources](https://bitbucket.org/search?q=repo%3Aarchitecture%20"TR-1"&account=%7Ba53a4df9-d55c-45a2-b5d4-ebb04c84ece1%7D)
# Ilities Family
.Data retention., .Privacy.
# labels
_accounting_