# Id
SDM Store [Level 2 Diagram](./sdm_container_level_2.puml)
# Role
datastore

# Description
Subscriber data implemented by DynamoDB

# Type
Hosted

# Container NFRs
| Req                                                       | Status | EnforcedBy | 
|-----------------------------------------------------------| ------ | ---------- |
| [TR-0035 - Readiness](../../tech_requirements/TR-0035.md) | IN_REVIEW |
| [TR-0036 - Liveness](../../tech_requirements/TR-0036.md)  | IN_REVIEW |

# Container Links
## From [prov_api](./prov_api.md)
| Direction  | Interaction | Protocol |
| ---------- | ----------- | -------- |
| Inbound  | blocking/synchronous  | HTTPs |

### NFRs

| Req                                                      | Status | EnforcedBy | 
|----------------------------------------------------------| ------ | ---------- |
| [TR-0036 - Liveness](../../tech_requirements/TR-0036.md) | IN_REVIEW |
