```plantuml
autonumber "<b>[000]"
title Traffic Controller Sequence
actor gtp_c
actor gtp_u
boundary messaging
control tcc
control tcu

actor spr
actor data_network
actor oss

== Initialization ==
gtp_c --> messaging: subscribe("gtpc.establish_response.<gtpc_worker_id>")
tcc --> messaging: subscribe("tcc.establish.*", tc_group)
note left: All TCC workers subscribe to same queue group for LB
tcc --> messaging: subscribe("tcc.establish_response.<tcc_worker_id)")
tcu --> messaging: subscribe("tcu.establish.*", tc_group)
note left: All TCU workers subscribe to same queue group for LB
== Session Establishment ==
gtp_c --> messaging: request("tcc.establish.<sessionid>", gtp_replyTo, tcc_establish_payload)
messaging --> tcc: nextMsg(tcc_establish_payload)
note left: Only one worker process msg
activate tcc
tcc --> spr: search_profile
spr --> tcc: profile
tcc --> messaging: request("tcu.establish.<sessionid>", tcc_replyTo, tcu_establish_payload)
messaging --> tcu: nextMsg(tcu_establish_payload)
note left: Only one worker process msg
tcu --> messaging: response(tcc_replyTo, <tcu_worker_id>)
messaging --> tcc: nextMsg(<tcu_worker_id>)
tcu --> messaging: response(gtp_replyTo, tcc_response_payload)
messaging --> tcu: nextMsg(tcc_response_payload)
deactivate tcc
```