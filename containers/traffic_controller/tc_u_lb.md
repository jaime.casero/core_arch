# Id
Traffic Controller User Load Balancer
# Role
Load Balancer

# Description
Routes User data plane traffic to appropiate TC_U worker based on incoming packet header

# Type
Owned

# Container Tech-Requirements
| Req                                                         | Status | EnforcedBy | 
|-------------------------------------------------------------| ------ |------------|
| [TR-0011 - AutoScaling](../../tech_requirements/TR-0011.md) | UnderReview  | K8s        |
| [TR-0035 - Readiness](../../tech_requirements/TR-0035.md)      | UnderReview  | K8s        |
| [TR-0036 - Liveness](../../tech_requirements/TR-0036.md)       | UnderReview  | K8s        |

# Container Links
## GTP-U
| Direction  | Interaction | Protocol |
| ---------- | ----------- | -------- |
| Inbound  | asynchronous  | GENEVE |

### Tech-Requirements
| Req  | Status | EnforcedBy | 
| ---- |--------| ---------- |
| [TR-0033 - Encryption In-Transit](../../tech_requirements/TR-0033.md) | TLS    |

## TC-U
| Direction | Interaction | Protocol |
|-----------| ----------- | -------- |
| Outbound  | asynchronous  | GENEVE |

### Tech-Requirements
| Req                                                                | Status       | EnforcedBy  | 
|--------------------------------------------------------------------|--------------|-------------|
| [TR-0033 - Encryption In-Transit](../../tech_requirements/TR-0033.md) | UnderReview     | TLS         |
| [TR-0017 - Graceful shutdown](../../tech_requirements/TR-0017.md)     | UnderReview | Custom Impl |

