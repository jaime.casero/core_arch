```plantuml
title Level 2: Container diagram
actor iot_device
actor iot_app
interface bss 
interface pgw
interface spr


package 'TC' {
    control tc_c [[https://gitlab.com/jaime.casero/core_arch.git/containers/traffic_controller/tc_c.md]]
    control tc_u [[https://gitlab.com/jaime.casero/core_arch.git/containers/traffic_controller/tc_u.md]]
    database tc_state_store [[https://gitlab.com/jaime.casero/core_arch.git/containers/traffic_controller/tc_c_state_store.md]]
    boundary tc_u_lb [[https://gitlab.com/jaime.casero/core_arch.git/containers/traffic_controller/tc_u_lb.md]]
    tc_c --> tc_state_store: modify_session_state [Redis]
    tc_c --> tc_u: assign worker [gRPC]
    database spr_store
    
}



iot_device --> pgw: establish_session
pgw --> tc_c: establish_session [gRPC]
bss --> tc_c: set_usage_trigger [Kafka]
tc_c --> bss: usage_trigger [Kafka]
pgw --> tc_u_lb: send_data [GENEVE]
tc_u_lb --> tc_u: send_data [GENEVE]
tc_u --> iot_app: send_data [IPv4]
```