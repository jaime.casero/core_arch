```plantuml
title Level 2: Container diagram
actor iot_device
actor iot_app



package 'GTP' {
    boundary sigtran_gtw
    boundary diameter_gtw
    control gtp_c [[https://gitlab.com/jaime.casero/core_arch.git/containers/gtp/gtp_c.md]]
    control gtp_u [[https://gitlab.com/jaime.casero/core_arch.git/containers/gtp/gtp_u.md]]
    gtp_c --> gtp_u: assign worker [gRPC]
}

interface tc



iot_device --> sigtran_gtw: establish_session [SS7]
iot_device --> diameter_gtw: establisg_session [DIAMETER]
sigtran_gtw --> gtp_c: establish_session [gRPC]
diameter_gtw --> gtp_c: establish_session [gRPC]
gtp_c --> tc: establish_session [gRPC]

gtp_u --> tc: send_data [GENEVE]
tc --> iot_app: send_data [IPv4]
```