# Id

# Role
balancer|service_logic|middleware|datastore

# Description

# Type
Hosted|Owned

# Container Tech-Requirements
| Req  | Status | EnforcedBy | 
| ---- | ------ | ---------- |
| [TR-0035 - Readiness](../tech_requirements/TR-0035.md) | IN_REVIEW |
| [TR-0036 - Liveness](../tech_requirements/TR-0036.md) | IN_REVIEW |

# Container Links
##<ServiceB link>
| Direction  | Interaction | Protocol |
| ---------- | ----------- | -------- |
| Inbound  | blocking/synchronous  | HTTP |

### Tech-Requirements
| Req  | Status | EnforcedBy | 
| ---- | ------ | ---------- |
| [TR-0036 - Liveness](../tech_requirements/TR-0036.md) | IN_REVIEW |
